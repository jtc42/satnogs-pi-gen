#!/bin/bash -e

install -m 644 files/satnogs-setup.conf ${ROOTFS_DIR}/etc/default/satnogs-setup

install -m 755 files/satnogs-setup.sh ${ROOTFS_DIR}/usr/local/bin/satnogs-setup

install -d ${ROOTFS_DIR}/usr/local/share/satnogs-setup
install -m 755 files/bootstrap.sh ${ROOTFS_DIR}/usr/local/share/satnogs-setup/bootstrap.sh

install -m 644 files/hosts ${ROOTFS_DIR}/etc/ansible/hosts
